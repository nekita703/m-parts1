export default class Star {
    reverse = '[data-reverse]';

    constructor() {
        console.log($(this.reverse));
        this.events();
    }

    starPaint($this) {
        let starSvg = $this.closest($('[data-star]'));
        if (starSvg[0].classList[1] == 'star') {
            console.log('staaar!');
            starSvg[0].classList.remove('star');
            starSvg[0].classList.add('star-checked');
        } else {
            starSvg[0].classList.remove('star-checked');
            starSvg[0].classList.add('star');
        }
    }

    events() {
        let self = this;

        $(document).on('click', '[data-star]', function (e) {
            e.preventDefault();
            self.starPaint($(this));
        });
    }
}

