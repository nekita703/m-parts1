export default class ValueChecker {
    reverse = '[data-check]';


    constructor() {
        console.log($(this.reverse));
        this.events();
    }

    plus($this) {
        let data = $this.closest($('[value-check-plus]'));
        // console.log('data[0]',data[0]);

        data[0].previousElementSibling.stepUp();
        // data[0].previousElementSibling.onchange();
    }

    minus($this){
        let data = $this.closest($('[value-check-minus]'));
        // console.log('data[0]',data[0]);

        data[0].nextElementSibling.stepDown();
        // data[0].nextElementSibling.onchange();
    }



    events() {
        let self = this;

        $(document).on('click', '[value-check-plus]', function (e) {
            e.preventDefault();
            self.plus($(this));
        });

        $(document).on('click', '[value-check-minus]', function (e) {
            e.preventDefault();
            self.minus($(this));
        });
    }
}

