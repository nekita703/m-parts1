export default class selectCustom {
    over = '[data-select-over]';
    select = '[data-select]';
    input = '[data-select-input]';
    button = '[data-select-button]';
    dropdown = '[data-select-dropdown]';
    option = '[data-select-option]';

    constructor() {
        console.log('runing select');
        this.on();
    }

    selectOpen($this) {
        var
            $select = $this.closest(this.select),
            $dropdown = $select.find(this.dropdown),
            $selectOuther = $select.closest(this.over).siblings().find(this.select),
            $dropdownOuther = $selectOuther.find(this.dropdown);

        $selectOuther.removeClass('is-open');
        $dropdownOuther.hide();

        if (!$select.hasClass('is-open')) {
            $select.addClass('is-open');
            $dropdown.show();
        } else {
            $select.removeClass('is-open');
            $dropdown.hide();
        }

    }

    selectClose(e) {
        var target = e.target;
        var $select = $(this.select);
        var $dropdown = $(this.dropdown);

        if ($select.hasClass('is-open') && !$select.is(target) && $select.has(target).length === 0) {
            $select.removeClass('is-open');
            $dropdown.hide();
            e.stopPropagation();
        }
    }

    selectOption($this) {
        var
            $value = $this.attr('data-value'),
            $select = $this.closest(this.select),
            $dropdown = $select.find(this.dropdown),
            $button = $select.find(this.button).find('span'),
            $input = $select.find(this.input),
            $options = $this.siblings($this);

        if ($value === 'new-order') {
            $select.removeClass('is-open');
            $dropdown.hide();
            return
        }

        $this.addClass('is-current');
        $options.removeClass('is-current');
        $button.text($value);
        $input.val($value);
        $select.removeClass('is-open');
        $dropdown.hide();
    }

    on() {
        console.log('runing select');
        var self = this;
        $(document).on('click', this.button, function () {
            self.selectOpen($(this));
        });
        $(document).on('click', this.option, function () {
            self.selectOption($(this));
        });
        $(document).on('click', function(e) {
            self.selectClose(e);
        });
    }
};
