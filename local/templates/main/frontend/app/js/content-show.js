export default class ContentShow {
    reverse = '[data-reverse]';
    table = '[data-table]';
    pr = '[data-pr]';
    row = '[data-row]';
    obj = '[data-reverse-toggle]';
    block = '[data-reverse-block]';

    constructor() {
        this.events();
    }

    reverseToggle($this) {
        let obj = $this.closest($('[data-reverse-toggle]'))[0];


        if (obj.classList[2] == "blue-color") {
            obj.classList.remove('blue-color');
            obj.childNodes[3].classList.remove('reverse-h');

        } else {
            // Свернуть
            obj.classList.add('blue-color');
            obj.childNodes[3].classList.add('reverse-h');
        }
    }

    handlerToggleTable($this) {

        let container = $this.closest($('[data-toggle-table]'));
        let content = container.find($('[data-toggle-table-content]'));
        let trigger = $this.closest($('[data-toggle-table-trigger]'));

        if (!container.hasClass('hide')) {
            trigger[0].classList.add('blue-color');
            trigger[0].childNodes[3].classList.add('reverse-h');
            trigger[0].childNodes[1].innerHTML = 'Показать цены';

            container.addClass('hide');
            content.slideUp(300);
        } else {
            trigger[0].classList.remove('blue-color');
            trigger[0].childNodes[3].classList.remove('reverse-h');
            trigger[0].childNodes[1].innerHTML = 'Свернуть';


            container.removeClass('hide');
            content.slideDown(300);
        }
    }

    events() {
        let self = this;

        $(document).on('click', '[data-toggle-table-trigger]', function(e) {
            e.preventDefault();

            self.handlerToggleTable($(this));

        });

        $(document).on('click', '[data-reverse-toggle]', function(e) {
            // e.preventDefault();
            // console.log('this= ',obj);
            // self.handler($(this));
            // self.reverseToggle(this);
            console.log('toggle= ',this);
            self.reverseToggle($(this));
        });
    }
}

