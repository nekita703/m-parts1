export default class Filter {
    reverse = '[data-reverse]';
    table = '[data-table]';
    pr = '[data-pr]';
    row = '[data-row]';

    constructor() {
        console.log($(this.reverse));
        this.events();
    }

    starPaint($this) {
        let starSvg = $this.closest($('[data-star]'));
        if (starSvg[0].classList[1] == 'star') {
            // console.log('staaar!');
            starSvg[0].classList.remove('star');
            starSvg[0].classList.add('star-checked');
        } else {
            starSvg[0].classList.remove('star-checked');
            starSvg[0].classList.add('star');
        }
    }

    selectFilter(a){
        let via = document.getElementById('filter0');
        let via1 = document.getElementById('filter1');
        if (!a) {
            console.log('Первый');
            via.classList.add('selected-filter');
            via1.classList.remove('selected-filter');
        }
        else{
            console.log('Второй');
            via1.classList.add('selected-filter');
            via.classList.remove('selected-filter');
        }
    }

    events() {
        let self = this;

        $(document).on('click', '[data-star]', function (e) {
            e.preventDefault();
            self.starPaint($(this));
        });
    }
}

