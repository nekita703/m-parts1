let formValidation = {

    run() {
        var self = this;
        let formV = 'form[data-form-validation]';
        $(document).on('submit', formV, function () {
            let form = $(this).get(0);
            let validate = self.validate(form);
            if (form.hasAttribute('data-form-no-ajax'))
                return validate;
            else
                return false;
        });
    },
    /**
     * @param form
     * @returns {boolean}
     */
    validate(form) {
        if (!form) {
            return true;
        }

        let self = this;
        let flag = true;

        let formError = form.querySelector('[data-form-error]');
        if (formError && formError.classList) {
            formError.classList.add('hidden');
        }

        let itemsError = form.querySelectorAll('[data-form-error]');
        if (itemsError && itemsError.length) {
            //itemsError.forEach(function (itemError) {
            for (var i = 0; i < itemsError.length; i++) {
                var itemError = itemsError[i];
                itemError.classList.add('hidden');
            };
        }

        let formSuccess = form.querySelectorAll('[data-form-success]');
        if (formSuccess && formSuccess.classList) {
            formSuccess.classList.add('hidden');
        }

        let inputs = form.querySelectorAll('input[type=number], input[type=text], input[type=radio], input[type=checkbox], textarea, input[type=password], select');
        if (inputs && inputs.length) {
            //inputs.forEach(function (input) {
            for (var i = 0; i < inputs.length; i++) {
                var input = inputs[i];
                let itemFlag = true;

                if (input.getAttribute('data-skip-validation') !== undefined && input.getAttribute('data-skip-validation') !== null) {
                    return;
                }

                let formItem = input.closest('[data-f-item]');
                let fieldsError;
                let errorItems;
                if (formItem) {
                    fieldsError = formItem.querySelectorAll('[data-form-error]');
                    errorItems = formItem.querySelectorAll('[data-error-item]');
                }

                let optionValue;
                let val = (input.nodeName === 'SELECT' && !(optionValue = input.options[input.selectedIndex].value)) ? optionValue : input.value;
                let typeInput = input.getAttribute('type');
                if (typeInput === 'checkbox') {
                    val = (input.checked === true);
                }

                if (val && input.getAttribute('data-form-field-phone')) {
                    val = val.split(' ').join('');
                    input.val(val);
                }
                if (
                    itemFlag && input.getAttribute('data-required') === 'Y'
                    && (
                        val === 'null'
                        || val == null
                        || val === undefined
                        || val === ''
                        || val === input.getAttribute('placeholder')
                        || val.length < 1
                        || val === false
                    )
                ) {
                    //errorItems.forEach(function (item) {
                    for (var i = 0; i < errorItems.length; i++) {
                        var item = items[i];
                        item.classList.add('hidden');
                        // item.innerHtml = 'Поле обязательно для заполнения';
                    };
                    let errorItem = formItem.querySelector('[data-error-item]');
                    if (errorItem && errorItem.classList) {
                        errorItem.classList.remove('hidden');
                    }
                    itemFlag = false;
                }

                if (
                    itemFlag && val && input.getAttribute('data-min-length') !== undefined && input.getAttribute('data-min-length') !== null
                    && val.length < input.getAttribute('data-min-length')
                ) {
                    //errorItems.forEach(function (item) {
                    for (var i = 0; i < errorItems.length; i++) {
                        var item = errorItems[i];
                        item.classList.add('hidden');
                        // item.innerHtml = 'Недостаточно символов';
                    };
                    let errorItem = formItem.querySelector('[data-error-item]');
                    if (errorItem && errorItem.classList) {
                        errorItem.classList.remove('hidden');
                    }
                    itemFlag = false;
                }

                if (
                    itemFlag && val && input.getAttribute('data-min-value') !== undefined && input.getAttribute('data-min-value') !== null
                    && parseInt(String(val).replace(' ', '')) < parseInt(input.getAttribute('data-min-value'))
                ) {
                    for (var i = 0; i < errorItems.length; i++) {
                        var item = errorItems[i];
                        // errorItems.forEach(function (item) {
                        item.classList.add('hidden');
                        // item.innerHtml = 'Неверно задано минимальное значение';
                    };
                    let errorItem = formItem.querySelector('[data-error-item]');
                    if (errorItem && errorItem.classList) {
                        errorItem.classList.remove('hidden');
                    }
                    itemFlag = false;
                }

                if (
                    itemFlag && val && input.getAttribute('data-max-value') !== undefined && input.getAttribute('data-max-value') !== null
                    && parseInt(String(val).replace(' ', '')) > parseInt(input.getAttribute('data-max-value'))
                ) {
                    for (var i = 0; i < errorItems.length; i++) {
                        var item = errorItems[i];
                        //errorItems.forEach(function (item) {
                        item.classList.add('hidden');
                        // item.innerHtml = 'Неверно задано максимальное значение';
                    };
                    let errorItem = formItem.querySelector('[data-error-item]');
                    if (errorItem && errorItem.classList) {
                        errorItem.classList.remove('hidden');
                    }
                    itemFlag = false;
                }

                if (
                    itemFlag && val && input.getAttribute('data-confirm-pass') !== undefined && input.getAttribute('data-confirm-pass') !== null
                    && val !== form.querySelector('[data-pass]').value
                ) {
                    for (var i = 0; i < errorItems.length; i++) {
                        var item = errorItems[i];
                        //errorItems.forEach(function (item) {
                        item.classList.add('hidden');
                        // item.innerHtml = 'Неверное подтверждение пароля';
                    };
                    let errorItem = formItem.querySelector('[data-error-item]');
                    if (errorItem && errorItem.classList) {
                        errorItem.classList.remove('hidden');
                    }
                    itemFlag = false;
                }

                if (
                    itemFlag && val && input.getAttribute('data-form-field-phone') !== undefined && input.getAttribute('data-form-field-phone') !== null
                    && ((/^\+7\s\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/).test(val) || (/^\d{11}$/).test(val))
                ) {
                    for (var i = 0; i < errorItems.length; i++) {
                        var item = errorItems[i];
                        //errorItems.forEach(function (item) {
                        item.classList.add('hidden');
                        // item.innerHtml = 'Некорректный формат номера телефона';
                    };
                    let errorItem = formItem.querySelector('[data-error-item]');
                    if (errorItem && errorItem.classList) {
                        errorItem.classList.remove('hidden');
                    }
                    itemFlag = false;
                }

                if (
                    itemFlag && val && input.getAttribute('data-form-field-email') !== undefined && input.getAttribute('data-form-field-email') !== null
                    && !self.validEmailTest(val)
                ) {
                    for (var i = 0; i < errorItems.length; i++) {
                        var item = errorItems[i];
                        //errorItems.forEach(function (item) {
                        item.classList.add('hidden');
                        // item.innerHtml = 'Введите корректный email';
                    };
                    let errorItem = formItem.querySelector('[data-error-item]');
                    if (errorItem && errorItem.classList) {
                        errorItem.classList.remove('hidden');
                    }
                    itemFlag = false;
                }

                if (itemFlag) {
                    if (input) {
                        input.classList.remove('error');
                    }
                    if (formItem) {
                        formItem.classList.remove('error');
                    }
                    if (fieldsError && fieldsError.length) {
                        for (var i = 0; i < fieldsError.length; i++) {
                            var fieldError = fieldsError[i];
                            // fieldsError.forEach(function (fieldError) {
                            fieldError.classList.add('hidden');
                        };
                    }
                    if (errorItems && errorItems.length) {
                        for (var i = 0; i < errorItems.length; i++) {
                            var errorItem = errorItems[i];
                            //errorItems.forEach(function (errorItem) {
                            errorItem.classList.add('hidden');
                        };
                    }

                } else {
                    if (input) {
                        input.classList.add('error');
                    }
                    if (formItem) {
                        formItem.classList.add('error');
                    }
                    if (fieldsError && fieldsError.length) {
                        for (var i = 0; i < fieldsError.length; i++) {
                            var fieldError = fieldsError[i];
                            //fieldsError.forEach(function (fieldError) {
                            fieldError.classList.add('hidden');
                        };
                    }
                    flag = false;
                }
            };
        }

        if (!flag) {
            self.scrollToMessage(form);
        }
        if (flag) $(form).trigger('form::valid');
        return flag;
    },
    /**
     * @param container
     */
    scrollToMessage(container) {
        let elScroll;

        let firstError;
        let errorsNode = container.querySelectorAll('.error');
        if (errorsNode && errorsNode.length) {
            //errorsNode.forEach(function (errorNode) {
            for (var i = 0; i < errorsNode.length; i++) {
                var errorNode = errorsNode[i];
                if (!firstError && errorNode.classList.contains('hidden') === false) {
                    firstError = errorNode;
                }
            };
        }

        if (firstError) {
            elScroll = firstError;
        }

        if (!elScroll) {
            let successNode = container.querySelector('.form--success');
            if (successNode && successNode.classList.contains('hidden') === false) {
                elScroll = successNode;
            }
        }

        if (elScroll) {
            elScroll.scrollIntoView({block: "center", behavior: "smooth"});
        }

    },

    validEmailTest(mail) {
        let re_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re_email.test(mail);
    }
}

module.exports = formValidation;
