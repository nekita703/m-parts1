export default class changeArrowFooter {
    reverse = '[data-reverse]';

    constructor() {
        // console.log($(this.reverse));
        this.events();
    }

    changeArrowFooterr($this) {
        let arrow = $this.closest($('[data-change-arrowFooter]'));
        // arrow = $this;
        // console.log('arrow= ', arrow[0].childNodes[3].classList);
        if (arrow[0].childNodes[3].classList[3] == "reverse-h") {
            arrow[0].childNodes[3].classList.remove('reverse-h');
        } else {
            arrow[0].childNodes[3].classList.add('reverse-h');
        }
    }

    events() {
        let self = this;

        $(document).on('click', '[data-change-arrowFooter]', function(e) {
            // e.preventDefault();
            // console.log('arrow= ', arrow);
            self.changeArrowFooterr($(this));
        });
    }
}

