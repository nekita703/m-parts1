export default class MapObject {
    constructor() {

        this.init();
        // setTimeout(this.init(), 1000);
    }

    init() {

        ($('#map').length) ? ymaps.ready(init) : false;
        ($('#map1').length) ? ymaps.ready(init1) : false;

        const self = this;
        let myMap, myMap1;
        let objectManager, objectManager1;
        let file = location.hostname === "localhost" || location.hostname === "127.0.0.1"
            ? '../images/icons/mark.svg'
            : '/mpart/images/icons/mark.svg';
        let url = location.hostname === "localhost" || location.hostname === "127.0.0.1"
            ? '../objectManager/data.json'
            : '/mpart/objectManager/data.json';

        function init() {
            console.log('starting init maps');
            myMap = new ymaps.Map("map", {
                center: [55.602671, 37.445729],
                zoom: 16,
                controls: []
            });
            // myMap1 = new ymaps.Map("map1", {
            //     center: [55.602671, 37.445729],
            //     zoom: 16,
            //     controls: []
            // });

            //1 MAP
            objectManager = new ymaps.ObjectManager({
                clusterize: true,
                gridSize: 6,
                clusterDisableClickZoom: true,
            });
            objectManager.objects.options.set({
                preset: 'islands#redIcon',
                iconLayout: 'default#image',
                iconImageHref: file
            });
            objectManager.clusters.options.set({
                preset:'islands#orangeClusterIcons'
            });

            //2 MAP
            objectManager1 = new ymaps.ObjectManager({
                clusterize: true,
                gridSize: 6,
                clusterDisableClickZoom: true,
            });
            objectManager1.objects.options.set({
                preset: 'islands#redIcon',
                iconLayout: 'default#image',
                iconImageHref: file
            });
            objectManager1.clusters.options.set({
                preset:'islands#orangeClusterIcons'
            });

            $.ajax({
                url: url
            }).done(function(data) {
                objectManager.add(data);
                // objectManager1.add(data);
                myMap.setBounds(myMap.geoObjects.getBounds(), {
                    checkZoomRange:true
                }).then(function(){
                    if(myMap.getZoom() > 14) myMap.setZoom(14);
                });
                // myMap1.setBounds(myMap1.geoObjects.getBounds(), {
                //     checkZoomRange:true
                // }).then(function(){
                //     if(myMap1.getZoom() > 14) myMap1.setZoom(14);
                // });
            });

            myMap.geoObjects.add(objectManager);
            // myMap1.geoObjects.add(objectManager1);

            function setObjectManager(type) {
                objectManager.setFilter(function (object) {
                    return object.properties.type == type;
                });
                objectManager1.setFilter(function (object) {
                    return object.properties.type == type;
                });
            }

            $(document).on('click', '[data-map-point-id]', function(e) {
                e.preventDefault();

                let
                    $page = $('html, body'),
                    $height = $('[data-header-sticky]').outerHeight(true),
                    $pointId = $(this).attr('data-map-point-id');

                setObjectManager($pointId);

                myMap.setBounds(myMap.geoObjects.getBounds(), {
                    checkZoomRange:true
                }).then(function(){
                    if(myMap.getZoom() > 14) myMap.setZoom(14);
                });

                myMap1.setBounds(myMap1.geoObjects.getBounds(), {
                    checkZoomRange:true
                }).then(function(){
                    if(myMap1.getZoom() > 14) myMap1.setZoom(14);
                });

                setTimeout(function () {
                    $page.animate({
                            scrollTop: $('#map').offset().top - ($height ? $height : "")  + "px"
                        },
                        {
                            duration: 700,
                            easing: ''
                        });
                    return false;
                }, 300);
            });
        }
        function init1() {
            console.log('starting init maps');

            myMap1 = new ymaps.Map("map1", {
                center: [55.602671, 37.445729],
                zoom: 16,
                controls: []
            });


            //2 MAP
            objectManager1 = new ymaps.ObjectManager({
                clusterize: true,
                gridSize: 6,
                clusterDisableClickZoom: true,
            });
            objectManager1.objects.options.set({
                preset: 'islands#redIcon',
                iconLayout: 'default#image',
                iconImageHref: file
            });
            objectManager1.clusters.options.set({
                preset:'islands#orangeClusterIcons'
            });

            $.ajax({
                url: url
            }).done(function(data) {
                // objectManager.add(data);
                objectManager1.add(data);

                myMap1.setBounds(myMap1.geoObjects.getBounds(), {
                    checkZoomRange:true
                }).then(function(){
                    if(myMap1.getZoom() > 14) myMap1.setZoom(14);
                });
            });

            // myMap.geoObjects.add(objectManager);
            myMap1.geoObjects.add(objectManager1);

            function setObjectManager(type) {
                objectManager.setFilter(function (object) {
                    return object.properties.type == type;
                });
                objectManager1.setFilter(function (object) {
                    return object.properties.type == type;
                });
            }

            $(document).on('click', '[data-map-point-id]', function(e) {
                e.preventDefault();

                let
                    $page = $('html, body'),
                    $height = $('[data-header-sticky]').outerHeight(true),
                    $pointId = $(this).attr('data-map-point-id');

                setObjectManager($pointId);


                myMap1.setBounds(myMap1.geoObjects.getBounds(), {
                    checkZoomRange:true
                }).then(function(){
                    if(myMap1.getZoom() > 14) myMap1.setZoom(14);
                });

                setTimeout(function () {
                    $page.animate({
                            scrollTop: $('#map').offset().top - ($height ? $height : "")  + "px"
                        },
                        {
                            duration: 700,
                            easing: ''
                        });
                    return false;
                }, 300);
            });
        }

    }
}